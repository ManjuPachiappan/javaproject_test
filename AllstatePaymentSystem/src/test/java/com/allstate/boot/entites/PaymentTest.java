package com.allstate.boot.entites;

import com.allstate.boot.entities.Payment;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

public class PaymentTest {

    @Test
    public void testPaymentParameterisedConstrutor() {
        Date todayDate = new Date();
        Payment myPayment = new Payment(1, todayDate, "upi", 342.35, 12345);
        assertEquals(342.35,myPayment.getAmount());
        assertEquals(12345,myPayment.getCustid());
        assertEquals(1,myPayment.getId());
        assertEquals("upi",myPayment.getType());
        assertEquals(todayDate,myPayment.getPaymentdate());

    }

    @Test
    public void testPaymentDefaultConstrutor() {
        Date todayDate = new Date();
        Payment myPayment = new Payment();
        myPayment.setAmount(342.35);
        myPayment.setPaymentdate(todayDate);
        myPayment.setCustid(1234);
        myPayment.setType("upi");
        myPayment.setId(1);
        assertEquals(342.35,myPayment.getAmount());
        assertEquals(1234,myPayment.getCustid());
        assertEquals(1,myPayment.getId());
        assertEquals("upi",myPayment.getType());
        assertEquals(todayDate,myPayment.getPaymentdate());

    }
}
