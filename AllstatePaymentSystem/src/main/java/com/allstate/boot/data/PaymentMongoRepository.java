package com.allstate.boot.data;

import com.allstate.boot.entities.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PaymentMongoRepository extends MongoRepository<Payment, Integer> {
    public List<Payment> findByType(String type);
    public Payment findById(int id);
}
