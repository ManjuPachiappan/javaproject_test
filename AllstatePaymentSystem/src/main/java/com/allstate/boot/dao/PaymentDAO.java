package com.allstate.boot.dao;

import com.allstate.boot.entities.Payment;
import org.springframework.stereotype.Component;

import java.util.List;

public interface PaymentDAO {

    public int rowcount();

    public Payment findById(int id);

    public List<Payment> findByType(String type);

    public Payment save(Payment payment);
}
